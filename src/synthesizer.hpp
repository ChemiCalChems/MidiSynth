#pragma once

#include <array>
#include <functional>

const unsigned short nh = 32;

class Synthesizer {
	static std::array<unsigned char, 128> key_velocities;
	static std::array<double, 16384> harmonics;
	
public:
	static double t;

	static double pitch_bend_semitones;
	static void set_patch(std::function<double(int)> f);
	static float get_sample(double t);
	static void set_key_velocity(unsigned int key, unsigned char velocity);
	static unsigned char get_key_velocity(unsigned int key);
};
