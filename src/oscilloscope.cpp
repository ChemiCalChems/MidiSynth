#include "oscilloscope.hpp"
#include <vector>
#include <SFML/Graphics/Vertex.hpp>
#include <iostream>

void Oscilloscope::render_wave_form() {
	this->clear(sf::Color::Black);
	double t = Synthesizer::t;

	double zero_crossing_time = t - 0.01; //start looking for zero crossing 10ms back

	while (std::abs(Synthesizer::get_sample(zero_crossing_time)) > 0.01 || Synthesizer::get_sample(zero_crossing_time + 0.001) < 0) {
		zero_crossing_time -= 0.0001;
	}
	
	for (unsigned int x = 0; x < this->getSize().x - 1; x++) {	
		double t1 = zero_crossing_time + x*0.05/this->getSize().x, t2 = zero_crossing_time + (x+1)*0.05/this->getSize().x;

		double amp1 = Synthesizer::get_sample(t1), amp2 = Synthesizer::get_sample(t2);
		sf::Vertex line [2] = {
			sf::Vertex{sf::Vector2f(x,   this->getSize().y/2 - 1/2.f*amp1*(this->getSize().y/2)), sf::Color::Green},
			sf::Vertex{sf::Vector2f(x+1, this->getSize().y/2 - 1/2.f*amp2*(this->getSize().y/2)), sf::Color::Green}
		};

		this->draw(line, 2, sf::Lines);
	}
	this->display();
}
