#pragma once

double sawtooth(int n) {
	if (n%2 == 0) return -1.f/3.14159268/(float)n;
	return 1.f/3.14159268/(float)n;
}

double square(int n) {
	if (n%2 == 0) return 0;
	return 4.f/(float)n/3.14159268;
}

double triangle(int n) {
	if (n%2 == 0) return 0;
	if ((n-1)%4 == 0) return 8.f/((float)n*(float)n)/(3.14159268*3.14159268);
	return -8.f/((float)n*(float)n)/(3.14159268*3.14159268);
}

double sine(int n) {
	if (n==1) return 1;
	return 0;
}

double u(int n) {
	return 1/(float)n/(float)n;
}

double a(int n) {
	switch(n) {
	case 1: return 1;
	case 2: return 0.7943282347242815;
	case 3: return 0.44668359215096315;
	case 4: return 0.44668359215096315;
	case 5: return 0.44668359215096315;
	case 6: return 1.2589254117941673;
	case 7: return 1.9952623149688795;
	case 8: return 3.1622776601683795;
	case 9: return 1.7782794100389228;
	case 10: return 0.7079457843841379;
	case 11: return 0.44668359215096315;
	case 12: return 1;
	case 13: return 1.9952623149688795;
	}
}

double none(int n) {
	return 0;
}

std::array<std::function<double(int)>, 256> patches;

