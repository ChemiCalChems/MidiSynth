#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include "synthesizer.hpp"

class Oscilloscope : public sf::RenderWindow {
	unsigned int time_span = 10; //milliseconds of resolution the oscilloscope will have
public: 
	void render_wave_form();
	Oscilloscope() {
		create(sf::VideoMode(1920, 1080), "test");
		//initialize();
	}
};
