#include <boost/asio.hpp>
#include <queue>
#include <array>
#include <iostream>
#include <thread>
#include "synthesizer.hpp"
#include <iomanip>
#include <Vc/Vc>
#include <RtAudio.h>
#include <future>
#include <sndfile.h>
#include "oscilloscope.hpp"
#include "patches.hpp"
#include <SFML/Window.hpp>

bool running = true;

unsigned char nibble(unsigned char byte, bool first = true) {
	return first ? byte >> 4 : byte % 16;
}

void midi_input(boost::asio::ip::tcp::socket* socket) {
	std::queue<unsigned char> bytes_read;
	
	while(running) {
		while(socket->available() > 0) {
			char byte;
			socket->read_some(boost::asio::buffer(&byte, 1));
			if (byte != 254) bytes_read.push(byte);
			
		}
		bool keepReading = true;	
		while (keepReading && !bytes_read.empty()) {
			auto byte = bytes_read.front();
			
			switch(nibble(byte)) {
			case 8: //1000 : noteOff
				{
					if (bytes_read.size() < 3) {
						keepReading = false;
						break;
					}
					bytes_read.pop();

					unsigned char key;
					key = bytes_read.front(); bytes_read.pop();
					/*velocity = bytes_read.front();*/ bytes_read.pop();
					Synthesizer::set_key_velocity(key, 0);
					break;
				}
			case 9: //1001 : noteOn
				{
					if (bytes_read.size() < 3) {
						keepReading = false;
						break;
					}
					bytes_read.pop();
					
					unsigned char key, velocity;
					key = bytes_read.front(); bytes_read.pop();
					velocity = bytes_read.front(); bytes_read.pop();
					Synthesizer::set_key_velocity(key, 128);
					break;
				}
			case 12: //1100 : programChange
				{
					if (bytes_read.size() < 2) {
						keepReading = false;
						break;
					}
					bytes_read.pop();

					unsigned char program = bytes_read.front();
					bytes_read.pop();
					std::cout << "setting" << (int)program << std::endl;
					Synthesizer::set_patch(patches.at(program));
					break;
				}
			case 14: //1110 : pitchbend
				{
					if (bytes_read.size() < 3) {
						keepReading = false;
						break;
					}
					bytes_read.pop();
					unsigned char lsb = bytes_read.front(); bytes_read.pop();
					unsigned char msb = bytes_read.front(); bytes_read.pop();

					short pitchbend = ((short)msb << 7) + lsb;
					double semitones = (double)(pitchbend - 8192) / 16834.f;
					Synthesizer::pitch_bend_semitones = semitones;
					break;
				}
			default: bytes_read.pop();
			}
		}
	}
}

SNDFILE* file;

int streamCallback (void* output_buf, void* input_buf, unsigned int frame_count, double time_info, unsigned int stream_status, void* userData) {
	if(stream_status) std::cout << "Stream underflow" << std::endl;
	float* out = (float*) output_buf;
	for (int i = 0; i<frame_count; i++) {
		float sample = Synthesizer::get_sample(Synthesizer::t);
		*out++ = sample;
		*out++ = sample;
		
		sf_writef_float(file, &sample, 1 /*frame*/);
		
		Synthesizer::t += (double)1/(double)48000;
		if (Synthesizer::t > 60) Synthesizer::t = 0;
	}
	return 0;
}

int Vc_CDECL main(int argc, char** argv) {
	patches[0] = sawtooth;
	patches[1] = square;
	patches[2] = triangle;
	patches[3] = sine;
	patches[4] = u;
	patches[5] = a;
	
	for (int i = 6; i<256; i++) {
		patches[i] = none;
	}
	
	Synthesizer::set_patch(patches[0]);
	
	boost::asio::io_service io_s;
	
	boost::asio::ip::tcp::socket socket{io_s};
	
	boost::asio::ip::tcp::endpoint endpoint;
	endpoint.address(boost::asio::ip::address::from_string("192.168.1.36"));
	endpoint.port(5001);
	
	socket.connect(endpoint);
	
	SF_INFO file_info;
	file_info.samplerate = 48000;
	file_info.channels = 1;
	file_info.format = SF_FORMAT_WAV | SF_FORMAT_FLOAT;

	file = sf_open("output.wav", SFM_WRITE, &file_info);
	
	std::thread io_service_thread ([&io_s](){io_s.run();});
	std::thread midi_thread(midi_input, &socket);

	RtAudio dac;
	
	RtAudio::StreamParameters params;
	params.deviceId = dac.getDefaultOutputDevice();
	params.nChannels = 2;
	params.firstChannel = 0;
	unsigned int buffer_length = 32;

	RtAudio::StreamOptions options;
	options.streamName = "MidiSynth OUT";
	
	dac.openStream(&params, nullptr, RTAUDIO_FLOAT32, std::atoi(argv[1]) /*sample rate*/, &buffer_length /*frames per buffer*/, streamCallback, nullptr, &options, nullptr);

	dac.startStream();

	Oscilloscope osc;
	
	//std::promise<void>().get_future().wait();
	while(true) {
		osc.render_wave_form();
	}
	running = false;

	dac.stopStream();
	
	io_service_thread.join();
	midi_thread.join();

	std::cout << sf_strerror(file) << std::endl;
	std::cout << sf_close(file) << std::endl;
}
