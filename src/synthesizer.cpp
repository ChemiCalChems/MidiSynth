#include "synthesizer.hpp"
#include "utils.hpp"
#include <ratio>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <thread>

#include <Vc/Vc>

std::array<double, 16384> Synthesizer::harmonics = {0};
std::array<unsigned char, 128> Synthesizer::key_velocities = {0};
double Synthesizer::t = 0;
double Synthesizer::pitch_bend_semitones = 0;

void Synthesizer::set_patch(std::function<double(int)> f) {
	for (int i=0; i<harmonics.size(); i++) {
		harmonics[i] = f(i+1);
	}
}

float Synthesizer::get_sample(double t) {
	Vc::float_v vector_result = Vc::float_v::Zero();
	float result = 0;
	for (int i = 0; i<key_velocities.size(); i++) {
		if (key_velocities.at(i) == 0) continue;
		//std::cout << "key" << i << std::endl;
		auto v = get_key_velocity(i);
		float f = utils::midi_to_note_freq(i) * std::pow(2, pitch_bend_semitones/12.f);
		int rendered_harmonics = 1;
		for (; rendered_harmonics*f < 20000 && rendered_harmonics <= 32; rendered_harmonics++);
		int j = 0;
		for (;j + Vc::float_v::size() <= rendered_harmonics; j+=Vc::float_v::size()) {
			Vc::float_v twopift = Vc::float_v::generate([f,t,j](int n){return 2*3.14159268*(j+n+1)*f*t;});
			Vc::float_v harms = Vc::float_v::generate([j](int n){return harmonics[n+j];});
			vector_result += v*harms*Vc::sin(twopift); 
			}
		for (; j < rendered_harmonics; j++) {
			result += (float)v*harmonics[j]*std::sin(2*3.14159268*(j+1)*f*t);
		}
	}
	return (result + vector_result.sum())/512;
}																								 

void Synthesizer::set_key_velocity(unsigned int key, unsigned char velocity) {
	key_velocities.at(key) = velocity;
}

unsigned char Synthesizer::get_key_velocity(unsigned int key) {
	return key_velocities[key];
}


